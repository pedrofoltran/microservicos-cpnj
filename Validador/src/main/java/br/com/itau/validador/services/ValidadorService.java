package br.com.itau.validador.services;

import br.com.itau.cadastro.model.Empresa;
import br.com.itau.validador.clients.EmpresaClient;
import br.com.itau.validador.models.DTOs.EmpresaSaidaGetClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ValidadorService {
    final BigDecimal UM_MILHAO = new BigDecimal(1000000);

    @Autowired
    EmpresaClient empresaClient;

    @Autowired
    KafkaTemplate<String, Empresa> kafkaTemplate;

    public void pesquisaCPNJ(String cnpj) {
        System.out.printf("CPNJ: "+ cnpj+"\n");
        EmpresaSaidaGetClient empresaSaidaGetClient = empresaClient.getPorCNPJ(cnpj);
        Empresa empresa = new Empresa();

        empresa.setCNPJ(cnpj);
        empresa.setCapitalSocial(empresaSaidaGetClient.getCapital_social());
        empresa.setNomeEmpresa(empresaSaidaGetClient.getFantasia());

        if (new BigDecimal(empresa.getCapitalSocial()).compareTo(UM_MILHAO) > 0) {
            System.out.printf("[MAIOR] Empresa: " + empresa.getNomeEmpresa() + " Capital: "
                    + empresa.getCapitalSocial() + " CPNJ: " + empresa.getCNPJ()+"\n");
            enviarKafkaLog(empresa);
        } else {
            System.out.printf("[MENOR] Empresa: " + empresa.getNomeEmpresa() + " Capital: "
                    + empresa.getCapitalSocial() + " CPNJ: " + empresa.getCNPJ()+"\n");
        }
    }

    public void enviarKafkaLog(Empresa empresa) {
        kafkaTemplate.send("spec3-pedro-dorighello-3", empresa);
    }
}
