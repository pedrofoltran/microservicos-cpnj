package br.com.itau.validador.models.DTOs;

public class EmpresaSaidaGetClient {
    private String fantasia;
    private String capital_social;

    public EmpresaSaidaGetClient() {
    }

    public String getFantasia() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }

    public String getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(String capital_social) {
        this.capital_social = capital_social;
    }
}
