package br.com.itau.validador.clients;

import br.com.itau.validador.models.DTOs.EmpresaSaidaGetClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "empresa", url = "https://www.receitaws.com.br/")
public interface EmpresaClient {

    @GetMapping("/v1/cnpj/{cnpj}")
    EmpresaSaidaGetClient getPorCNPJ(@PathVariable String cnpj);

}
