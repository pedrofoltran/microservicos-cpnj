package br.com.itau.validador.consumers;

import br.com.itau.cadastro.model.Empresa;
import br.com.itau.validador.services.ValidadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class ValidadorConsumer {
    @Autowired
    ValidadorService validadorService;

    @KafkaListener(topics = "spec3-pedro-dorighello-2", groupId = "pedro")
    public void validadorConsumer(@Payload Empresa empresa){
        validadorService.pesquisaCPNJ(empresa.getCNPJ());
    }
}
