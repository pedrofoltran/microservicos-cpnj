package br.com.itau.cadastro.controler;

import br.com.itau.cadastro.model.Empresa;
import br.com.itau.cadastro.service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/empresa")
public class EmpresaControler {

    @Autowired
    EmpresaService empresaService;

    @PostMapping("/{cnpj}")
    public Empresa cadastrarEmpresa(@PathVariable String cnpj) {
        return empresaService.enviarCadastro(cnpj);
    }
}
