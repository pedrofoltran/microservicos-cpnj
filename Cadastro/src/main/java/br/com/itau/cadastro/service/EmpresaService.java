package br.com.itau.cadastro.service;


import br.com.itau.cadastro.model.Empresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.EnumMap;

@Service
public class EmpresaService {
    @Autowired
    private KafkaTemplate<String, Empresa> kafkaTemplate;

    public Empresa enviarCadastro(String cnpj) {
        Empresa empresa = new Empresa();
        empresa.setCNPJ(cnpj);

        enviarAoKafka(empresa);
        return empresa;
    }

    private void enviarAoKafka(Empresa empresa) {
        kafkaTemplate.send("spec3-pedro-dorighello-2", empresa);
    }
}
