package br.com.itau.log;

import br.com.itau.cadastro.model.Empresa;
import br.com.itau.log.mappers.EmpresaMapper;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Paths;

@Component
public class EmpresaConsumer {

    @KafkaListener(topics = "spec3-pedro-dorighello-3", groupId = "pedro")
    public void escreveLog(@Payload Empresa empresa) {
        try {
            Writer writer = new FileWriter("log.csv", true);

            writer.write(EmpresaMapper.empresaToString(empresa));

            System.out.println("Log salvo em: " + Paths.get("log.csv"));
            writer.close();
        } catch (IOException ioException) {
            ioException.getMessage();
        }
    }
}
