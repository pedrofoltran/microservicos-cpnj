package br.com.itau.log.mappers;


import br.com.itau.cadastro.model.Empresa;

public class EmpresaMapper {
    public static String empresaToString(Empresa empresa) {
        String resposta = empresa.getCNPJ() + "," +
                empresa.getNomeEmpresa() + "," +
                empresa.getCapitalSocial() + "," + "\n";
        return resposta;
    }
}
